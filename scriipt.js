$(document).ready(function () {
  var LEFT = 37, RIGHT = 39;
  var boardWidth = 400;
  var racketWidth = 62;
  var ballWidth = 11;
  var ballHeight = 10;
  var racket = $('.racket');
  var frame = $('.frame');
  var $maBall = $('#ball');
  var sensX = 10;
  var sensY = 10;
  var sens = 10
  var boardLeft = 0;
  var boardTop = 0;
  var boardBottom = 400;

  function deplaceBalle() { 
    var positionR = racket.position();  
    var position = $maBall.position();
    // var pointLeft = position.left + ballWidth;
    // var pointTop = position.top + ballHeight;
    // var barLeftStart = positionR.left- racketWidth;
    // var barLeftEnd = barLeftStart + racketWidth;
    // var barTop = positionR.top + ballHeight;
    var posx = position.left + sensX;
    var posy = position.top + sensY;    
    $maBall.css(
      {
           left: posx + "px",
           top: posy + "px"
      });

      // si colision on rebondit
      if (colision($maBall, racket)) {
        sensY =-10;
        console.log('colision')
      }

      checkBrickColision()

      // if(pointTop>= barTop
      //   &&pointLeft>=barLeftStart
      //   &&pointLeft<=barLeftEnd){
      //     alert('wtf');
      //   }

    if ($maBall.position().left >= boardWidth-ballWidth){
      sensX =-10;
    }
    else if ($maBall.position().left <= (boardLeft)) {
      sensX = +10;
    }

    if ($maBall.position().top <= boardTop) {
      sensY =+10;
    }

    else if ($maBall.position().top >= boardBottom - ballHeight) {
      sensY =-10;
    }
  }

  setInterval(deplaceBalle, 100);



  $(document).keydown(function (e) {
    console.log('e:', e);

    switch (e.keyCode) {
      case LEFT:
        moveLeft()
        break;
      case RIGHT:
        moveRight()
        break;
    }
  });

  function moveLeft() {   
    var position = racket.position();
    var left = position.left - 10;
    left = Math.max(0, left);
    racket.css({ 'left': left + 'px' });
    // if (checkColision()) racket.css({ 'left': (left + 10) + 'px' });
  }

  function moveRight() {    
    var position = racket.position();
    var left = position.left + 10;
    left = Math.min((boardWidth - racketWidth), left);
    racket.css({ 'left': left + 'px' });
  //   if (checkColision()) racket.css({ 'left': (left - 10) + 'px' });
   }

  function colision(elt1, elt2) {
    var p_elt1 = elt1.position();
    var p_elt2 = elt2.position();

    if (p_elt1.left < p_elt2.left + elt2.width() &&
      p_elt1.left + elt1.width() > p_elt2.left &&
      p_elt1.top < p_elt2.top + elt2.height() &&
      elt1.height() + p_elt1.top > p_elt2.top) {
      return true;
    }
    return false
  }
  function checkBrickColision() {
    $('td').each(function () {
      if (colision($maBall, $(this))) {
        $(this).hide()
      }
    })}


});